import models.Circle;
import models.Rectangle;
import models.Shape;
import models.Square;

public class App {
    public static void main(String[] args) throws Exception {
        Shape shape1 = new Shape();
        System.out.println("Shape 1");
        System.out.println(shape1);

        Shape shape2 = new Shape("green", false);
        System.out.println("Shape 2");
        System.out.println(shape2);

        Circle circle1 = new Circle();
        System.out.println("Circle 1");
        System.out.println(circle1);
        System.out.println("dien tinh " + circle1.getArea());
        System.out.println("chu vi " + circle1.getPerimeter());
        
        Circle circle2 = new Circle(2.0);
        System.out.println("Circle 2");
        System.out.println(circle2);
        System.out.println("dien tinh " + circle2.getArea());
        System.out.println("chu vi " + circle2.getPerimeter());

        Circle circle3 = new Circle("green", true, 3.0);
        System.out.println("Circle 3");
        System.out.println(circle3);
        System.out.println("dien tinh " + circle3.getArea());
        System.out.println("chu vi " + circle3.getPerimeter());

        Rectangle rectangle1 = new Rectangle();
        System.out.println("Rectangle 1");
        System.out.println(rectangle1);
        System.out.println("dien tinh" + rectangle1.getArea());
        System.out.println("Chi vi " + rectangle1.getPerimeter());

        Rectangle rectangle2 = new Rectangle(2.5, 1.5);
        System.out.println("Rectangle 2");
        System.out.println(rectangle2);
        System.out.println("dien tinh" + rectangle2.getArea());
        System.out.println("Chi vi " + rectangle2.getPerimeter());

        Rectangle rectangle3 = new Rectangle("green", true, 2.0, 1.5);
        System.out.println("Rectangle 3");
        System.out.println(rectangle3);
        System.out.println("dien tinh" + rectangle3.getArea());
        System.out.println("Chi vi " + rectangle3.getPerimeter());

        Square square1 = new Square();
        System.out.println("Square 1");
        System.out.println(square1);
        System.out.println("Dien tich " + square1.getArea());
        System.out.println("Chu vi " + square1.getPerimeter());

        Square square2 = new Square(1.5);
        System.out.println("Square 2");
        System.out.println(square2);
        System.out.println("Dien tich " + square2.getArea());
        System.out.println("Chu vi " + square2.getPerimeter());

        Square square3 = new Square(2.0,"green", true);
        System.out.println("Square 3");
        System.out.println(square3);
        System.out.println("Dien tich " + square3.getArea());
        System.out.println("Chu vi " + square3.getPerimeter());
    }
}
