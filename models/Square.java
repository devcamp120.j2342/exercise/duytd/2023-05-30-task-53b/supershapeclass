package models;

public class Square extends Rectangle {
    private double side;

    public Square() {

    }

    public Square(double side) {
        this.side = side;
    }

    public Square(double side, String color, boolean filled) {
        this.getColor();
        this.isFilled();
        this.side = side;
    }

    public double getSide() {
        return side;
    }

    public void setSide(double side) {
        this.side = side;
    }

    public void setWidth(double width) {
        this.side = width;
    }

    public void setLength(double length) {
        this.side = length;
    }

    @Override
    public double getWidth() {
        return side;
    }

    @Override
    public double getLength() {
        return side;
    }

    @Override
    public String toString() {
        return "Square [Rectangle [Shape [color = " + this.getColor() + ", filled = " + this.isFilled() + "], width = " + this.getWidth() + ", length = " + this.getLength() + "]]";
    }
}
